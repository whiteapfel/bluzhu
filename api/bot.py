from os import environ

from aiogram import Bot

API_TOKEN = environ.get("TG_TOKEN")
bot = Bot(token=API_TOKEN)
