from db import connect, disconnect

if __name__ == "__main__":
    from dotenv import load_dotenv

    load_dotenv("../.env")

import uvicorn
from fastapi import FastAPI
from starlette.middleware.cors import CORSMiddleware

from api.manage import router as manage_router

app = FastAPI()

origins = [
    "http://localhost:8000",
    "http://localhost:8080",
    "http://127.0.0.1:8000",
    "http://127.0.0.1:8080",
    "*",
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


@app.get("/")
def index():
    return {"status": "dev"}


@app.on_event("startup")
async def on_startup():
    connect()


@app.on_event("shutdown")
async def event_shutdown():
    disconnect()


app.include_router(manage_router)

if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=8080)
