import time
from io import BytesIO

import asyncio
from fastapi import APIRouter, Depends, Path
from pydantic import BaseModel, Field
from starlette.responses import StreamingResponse

from bot import bot
from db import get_db
from storage import get_violations_media

router = APIRouter(prefix="/api")


class SendTgMessage(BaseModel):
    user_id: int
    message: str


@router.post("/send_tg_message")
async def send_tg_message(data: SendTgMessage, db=Depends(get_db)):
    if data.user_id == 0:
        users = await db.observers.find({"verified": True}).to_list(length=1000)
        for user in users:
            await bot.send_message(user['user_id'], data.message)
            await asyncio.sleep(.1)
    else:
        await bot.send_message(data.user_id, data.message, "HTML")
    return data


class ObserverVerify(BaseModel):
    uik: int
    name: str
    user_id: int
    verified: bool = Field(True)
    hide: bool = Field(False)


@router.post("/verify_observer")
async def verify_observer(observer: ObserverVerify, db=Depends(get_db)):
    updated = await db.observers.update_one(
        {"user_id": observer.user_id}, {"$set": observer.dict()}
    )
    await send_tg_message(
        SendTgMessage(
            user_id=observer.user_id,
            message="Проверка пройдена! Нажмите на команду /start, чтобы пользоваться ботом",
        )
    )
    return


@router.get("/uiks")
async def get_uiks(db=Depends(get_db)):
    uiks = await db.uiks.find({}).to_list(length=200)
    for i in range(len(uiks)):
        del uiks[i]["_id"]
    return uiks


class NewUik(BaseModel):
    uik: int
    additional: str = Field("")
    total_voters: int = Field(1000)
    counter_voters: int = Field(0)
    uik_voters: int = Field(0)
    voters_history: list = Field([])


@router.post("/uiks")
async def add_uik(new_uik: NewUik, db=Depends(get_db)):
    data = new_uik.dict()
    print(data)
    uik = await db.uiks.insert_one(data)
    return data


@router.get("/observers")
async def get_observers(db=Depends(get_db)):
    observers = await db.observers.find({"verified": True}).to_list(length=1000)
    for i in range(len(observers)):
        del observers[i]["_id"]
    return observers


@router.get("/observers/to_verify")
async def get_observers_to_verify(db=Depends(get_db)):
    observers = await db.observers.find({"verified": False}).to_list(length=1000)
    for i in range(len(observers)):
        del observers[i]["_id"]
    return observers


@router.get("/violations")
async def get_violations(db=Depends(get_db)):
    violations = await db.violations.find({}).to_list(length=1000)
    for i in range(len(violations)):
        del violations[i]["_id"]
    return violations


@router.get("/violations/{uik}")
async def get_violations(uik: int = Path(...), db=Depends(get_db)):
    violations = await db.violations.find({"uik": uik}).to_list(length=1000)
    for i in range(len(violations)):
        del violations[i]["_id"]
    return violations


@router.get("/timestamps")
async def get_timestamps(db=Depends(get_db)):
    timestamps = [
                 {
                     "start": 1631840400,
                     "finish": 1631849400,
                     "name": "Военное положение!"
                 },
                 {
                     "start": 1631849400,
                     "finish": 1631854200,
                     "name": "Фаза 1"
                 },
                 {
                     "start": 1631854200,
                     "finish": 1631861700,
                     "name": "Фаза 2"
                 },
                 {
                     "start": 1631861700,
                     "finish": 1631868900,
                     "name": "Фаза 3"
                 },
                 {
                     "start": 1631868900,
                     "finish": 1631879700,
                     "name": "Фаза 4"
                 },
                 {
                     "start": 1631879700,
                     "finish": 1631890500,
                     "name": "Фаза 5"
                 },
                 {
                     "start": 1631890500,
                     "finish": 1631897700,
                     "name": "Фаза 6"
                 },
                 {
                     "start": 1631897700,
                     "finish": 1631908800,
                     "name": "Фаза 7"
                 },
                 {
                     "start": 1631908800,
                     "finish": 1631935800,
                     "name": "Фаза 8"
                 },
                 {
                     "start": 1631935800,
                     "finish": 1631940600,
                     "name": "Фаза 9"
                 },
                 {
                     "start": 1631940600,
                     "finish": 1631948100,
                     "name": "Фаза 10"
                 },
                 {
                     "start": 1631948100,
                     "finish": 1631955300,
                     "name": "Фаза 11"
                 },
                 {
                     "start": 1631955300,
                     "finish": 1631966100,
                     "name": "Фаза 12"
                 },
                 {
                     "start": 1631966100,
                     "finish": 1631976900,
                     "name": "Фаза 13"
                 },
                 {
                     "start": 1631976900,
                     "finish": 1631984100,
                     "name": "Фаза 14"
                 },
                 {
                     "start": 1631984100,
                     "finish": 1631995200,
                     "name": "Фаза 15"
                 },
                 {
                     "start": 1631995200,
                     "finish": 1632022200,
                     "name": "Фаза 16"
                 },
                 {
                     "start": 1632022200,
                     "finish": 1632027000,
                     "name": "Фаза 17"
                 },
                 {
                     "start": 1632027000,
                     "finish": 1632034500,
                     "name": "Фаза 18"
                 },
                 {
                     "start": 1632034500,
                     "finish": 1632041700,
                     "name": "Фаза 19"
                 },
                 {
                     "start": 1632041700,
                     "finish": 1632052500,
                     "name": "Фаза 20"
                 },
                 {
                     "start": 1632052500,
                     "finish": 1632063300,
                     "name": "Фаза 21"
                 },
                 {
                     "start": 1632063300,
                     "finish": 1632070500,
                     "name": "Фаза 22"
                 },
                 {
                     "start": 1632070500,
                     "finish": 1632081600,
                     "name": "Фаза 23"
                 },
                 {
                     "start": 1631608464,
                     "finish": 1631840400,
                     "name": "Мирное время"
                 },
                 {
                     "start": 1631840400,
                     "finish": 1731840400,
                     "name": "Мирное время"
                 }
    ]
    return timestamps


@router.get("/yavka")
async def get_yavka(db=Depends(get_db)):
    uiks = await db.uiks.find({}).to_list(length=1000)
    counter = 0
    official = 0
    for uik in uiks:
        counter += uik["counter_voters"]
        official += uik["uik_voters"]
    return {"counter_voters": counter, "uik_voters": official}


async def get_current_phase(db):
    phases: list = await get_timestamps(db)
    now = time.time()
    print(now)
    phases = filter(lambda i: i["start"] <= now < i["finish"], phases)
    return list(phases)[0]


@router.get("/uiks/not_reported")
async def get_yavka(db=Depends(get_db)):
    uiks = await db.uiks.find({}).to_list(length=1000)
    phase = await get_current_phase(db)
    not_reported = []
    for uik in uiks:
        if not uik["voters_history"] or (
                uik["voters_history"]
                and uik["voters_history"][-1]["time"] < phase["start"]
        ):
            print(phase)
            del uik['_id']
            not_reported.append(uik)
    return not_reported


@router.get("/media/{filename:path}")
async def get_media(filename: str = Path(...), db=Depends(get_db)):
    print(filename)
    m = get_violations_media(filename)
    file = BytesIO()
    file.write(m.read())
    file.seek(0)
    m.close()
    m.release_conn()
    return StreamingResponse(file)
