from io import BytesIO
from os import environ

from minio import Minio

client = Minio("nginx:9000", environ.get('MINIO_ROOT_USER'), environ.get('MINIO_ROOT_PASSWORD'), secure=False)


def get_violations_media(name):
    if not client.bucket_exists("violations"):
        client.make_bucket("violations")
    result = client.get_object("violations", name[11:])
    return result
