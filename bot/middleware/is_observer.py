from aiogram import types, Dispatcher
from aiogram.dispatcher.handler import CancelHandler, current_handler
from aiogram.dispatcher.middlewares import BaseMiddleware
from aiogram.types import InlineKeyboardMarkup

from db import get_db


class IsObserverMiddleware(BaseMiddleware):
    def __init__(self):
        super(IsObserverMiddleware, self).__init__()

    async def on_process_message(self, message: types.Message, data: dict):
        # handler = current_handler.get()
        db = get_db()
        user = await db.observers.find({"user_id": message.from_user.id}).to_list(
            length=10
        )
        if not user:
            await message.reply(
                "Привет 👋\n\nМиша разрешил работать только с наблюдателями. "
                "Он сам будет проверять твой статус, так что можешь лишний раз не пытаться меня обмануть\n\n"
                "🧑‍⚖️ <b>Ты наблюдатель?</b>",
                parse_mode="HTML",
                reply_markup=InlineKeyboardMarkup().add(
                    types.InlineKeyboardButton(
                        text="Нет", callback_data="registration_no"
                    ),
                    types.InlineKeyboardButton(
                        text="Да", callback_data="registration_yes"
                    ),
                ),
            )
            user = await db.observers.insert_one(
                {
                    "user_id": message.from_user.id,
                    "verified": False,
                    "hide": False,
                    "uik": None,
                    "username": message.from_user.username,
                    "name": None,
                }
            )
            raise CancelHandler()
        elif user and not user[0]["verified"]:
            await message.delete()
            raise CancelHandler()
