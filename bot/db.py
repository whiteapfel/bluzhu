import os

import motor.motor_asyncio

db = None
client = None


def get_db() -> motor.motor_asyncio.AsyncIOMotorClient:
    global client
    if not client:
        connect()
    return client


def connect():
    global client
    global db
    db = motor.motor_asyncio.AsyncIOMotorClient(
        os.environ.get("MONGODB_URL", "localhosh:27017")
    )
    client = db.novgorod


def disconnect():
    global db
    db.close()
