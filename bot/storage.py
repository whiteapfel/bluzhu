from io import BytesIO
from os import environ

from minio import Minio

client = Minio("nginx:9000", environ.get('MINIO_ROOT_USER'), environ.get('MINIO_ROOT_PASSWORD'), secure=False)


def upload_violations_media(uploaded_object: BytesIO, name):
    if not client.bucket_exists("violations"):
        client.make_bucket("violations")
    result = client.put_object(
        "violations", name, uploaded_object, uploaded_object.getbuffer().nbytes
    )
    return result
