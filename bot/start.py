import logging

import dotenv

import db

dotenv.load_dotenv("../.env")

from aiogram.utils.executor import start_webhook, start_polling

from bot import bot, dp, WEBAPP_HOST, WEBAPP_PORT, WEBHOOK_PATH, WEBHOOK_URL


async def on_startup(dp):
    # await bot.set_webhook(WEBHOOK_URL)
    db.connect()
    ...


async def on_shutdown(dp):
    logging.warning("Shutting down..")
    db.disconnect()
    # insert code here to run it before shutdown

    # Remove webhook (not acceptable in some cases)
    # await bot.delete_webhook()

    # Close DB connection (if used)
    await dp.storage.close()
    await dp.storage.wait_closed()

    logging.warning("Bye!")


if __name__ == "__main__":
    # start_webhook(
    #     dispatcher=dp,
    #     webhook_path=WEBHOOK_PATH,
    #     on_startup=on_startup,
    #     on_shutdown=on_shutdown,
    #     skip_updates=True,
    #     host=WEBAPP_HOST,
    #     port=WEBAPP_PORT,
    # )
    start_polling(dp, on_startup=on_startup, on_shutdown=on_shutdown)
