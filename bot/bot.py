from os import environ

from aiogram import Bot
from aiogram.contrib.fsm_storage.mongo import MongoStorage
from aiogram.contrib.middlewares.logging import LoggingMiddleware
from aiogram.dispatcher import Dispatcher
import motor.motor_asyncio
import logging
import hashlib

from middleware.is_observer import IsObserverMiddleware

IP = "0.0.0.0"
API_TOKEN = environ.get("TG_TOKEN")
MD5_API_TOKEN = hashlib.md5(API_TOKEN.encode("utf-8")).hexdigest()

# webhook settings
WEBHOOK_HOST = f"http://localhost"
WEBHOOK_PATH = f"/{MD5_API_TOKEN}"
WEBHOOK_URL = f"{WEBHOOK_HOST}{WEBHOOK_PATH}"
# webserver settings
WEBAPP_HOST = "0.0.0.0"  # or ip
WEBAPP_PORT = 48501

logging.basicConfig(level=logging.DEBUG)

bot = Bot(token=API_TOKEN)

storage = MongoStorage(
    uri=environ.get("MONGODB_URL", "localhost:27017"), db_name="aiogram_fsm"
)

dp = Dispatcher(bot, storage=storage)
dp.middleware.setup(LoggingMiddleware())
dp.middleware.setup(IsObserverMiddleware())

from modules.main import *
from modules.registration import *
from modules.turnout import *
from modules.violations import *
