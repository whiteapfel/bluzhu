from aiogram import types
from aiogram.dispatcher import FSMContext

from bot import dp, bot


@dp.message_handler(commands=["cancel"], state="*")
async def cancel_command(message: types.Message, state: FSMContext):
    await state.reset_state()
    await message.reply("🚶 Мы вернулись в меню!")


@dp.message_handler(lambda m: m.text and m.text == "Памятки")
async def memo(message: types.Message):
    await message.answer("Тут будет ссылка на памятки")


@dp.message_handler(lambda m: m.text and m.text == "Образцы заявлений")
async def memo(message: types.Message):
    await message.answer("Тут будет ссылка на образцы заявлений")


@dp.message_handler(lambda m: m.text and m.text == "КАРАУЛ! ТРЕВОГА!")
async def memo(message: types.Message):

    await message.answer(
        "Точно уведомляем штаб о происходящем мракобесии?",
        reply_markup=types.InlineKeyboardMarkup().add(
            types.InlineKeyboardButton(text="Нет", callback_data="karaul_no"),
            types.InlineKeyboardButton(text="😱 ДА 😱", callback_data="karaul_yes"),
        ),
    )


@dp.callback_query_handler(lambda c: c.data and c.data.split("_")[0] == "karaul")
async def registration_callback(callback: types.CallbackQuery):
    if callback.data.split("_")[-1] == "yes":
        await callback.message.edit_text(
            "Уведомление направилось в штаб по выделенной полосе с мигалками 🚓"
        )
        await callback.answer("⌛ Ты попал в очередь на подтверждение")
    elif callback.data.split("_")[-1] == "no":
        await callback.message.delete()
        await callback.answer("Если что -- зови!")
    else:
        await callback.answer("Что-то пошло не по плану")
