import time
from hashlib import md5
from io import BytesIO

from aiogram import types
from aiogram.dispatcher import FSMContext
from aiogram.dispatcher.filters.state import State, StatesGroup
from bot import dp, bot
from db import get_db
from storage import upload_violations_media


class ViolationsFunnel(StatesGroup):
    waiting_for_messages = State()


@dp.message_handler(lambda m: m.text and m.text == "Вижу нарушение")
async def turnout_set(message: types.Message):
    await message.reply(
        f"Ты можешь отправить одно или несколько сообщений, приложить фото или видео \n"
        f"Пожалуйста, опиши детали, чтобы нам было проще принимать решения. "
        f"И в рамках одной заявки одно нарушение, плиз\n\n"
        f"📨 Как только закончишь, нажми на команду /send\n"
        f"🚫 Для выхода из режима доноса нажми на команду /cancel"
    )
    await ViolationsFunnel.waiting_for_messages.set()


@dp.message_handler(commands=["send"], state=ViolationsFunnel.waiting_for_messages)
async def cancel_command(message: types.Message, state: FSMContext):
    state_data = await state.get_data()
    messages = state_data.get("violations_messages", [])

    report = ""
    for m in messages:
        if m["text"]:
            report += f"{m['text']}\n"
        if m["filename"]:
            report += f"{m['filename']}\n\n"
        else:
            report += "\n"
    file_report = BytesIO()
    file_report.write(report.encode())
    file_report.seek(0)
    upload_violations_media(
        file_report,
        f"{message.from_user.username or message.from_user.id}/"
        f"{int(time.time())}.txt",
    )
    db = get_db()
    user = await db.observers.find_one({"user_id": message.from_user.id})
    await db.violations.insert_one(
        {
            "time": int(time.time()),
            "author": str(message.from_user.username or message.from_user.id),
            "report_final": report,
            "report_list": messages,
            "uik": user['uik']
        }
    )
    await message.reply(
        f"Отправляю репорт в штаб 🚀\n\n<code>{report}</code>", parse_mode="HTML"
    )
    await state.update_data(violations_messages=[])
    await state.reset_state(with_data=False)


async def upload_file(file, from_user):
    file_info = await bot.get_file(file.file_id)
    print(file_info.file_path.split("/")[-1].split(".")[-1])
    filename = (
        f"{from_user.username or from_user.id}/"
        f"{int(time.time())}_{md5(file.file_id.encode()).hexdigest()}"
        f".{file_info.file_path.split('/')[-1].split('.')[-1]}"
    )
    file = await file.download(BytesIO())
    result = upload_violations_media(file, filename)
    return filename


@dp.message_handler(
    state=ViolationsFunnel.waiting_for_messages,
    content_types=["photo", "video", "text", "document"],
)
async def violations_report_register(message: types.Message, state: FSMContext):
    state_data = await state.get_data()
    messages = state_data.get("violations_messages", [])
    print(message.content_type)
    filename = None
    if message.content_type == "photo":
        filename = await upload_file(message.photo[-1], message.from_user)
    if message.content_type == "video":
        filename = await upload_file(message.video, message.from_user)
    if message.content_type == "document":
        filename = await upload_file(message.document, message.from_user)
    data = {
        "text": message.text or message.caption,
        "filename": f"violations/{filename}" if filename else None,
    }

    messages.append(dict(data))
    await state.update_data(violations_messages=messages)
