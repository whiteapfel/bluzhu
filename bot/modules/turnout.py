import time

from aiogram import types
from aiogram.dispatcher import FSMContext
from aiogram.dispatcher.filters.state import State, StatesGroup
from bot import dp, bot
from db import get_db


class TurnoutFunnel(StatesGroup):
    waiting_for_counter = State()
    waiting_for_official = State()


@dp.message_handler(lambda m: m.text and m.text == "Явка")
async def turnout_set(message: types.Message):
    await message.reply(
        f"🕵️ Отправь явку, которую насчитал ты.\n\n"
        f""
        f"🚫 Если передумаешь, нажми на команду /cancel"
    )
    await TurnoutFunnel.waiting_for_counter.set()


@dp.message_handler(state=TurnoutFunnel.waiting_for_counter)
async def turnout_set_counter(message: types.Message, state: FSMContext):
    if not message.text.strip().isdecimal():
        await message.answer("Пожалуйста, введите число")
        return
    state_data = await state.get_data()
    if state_data.get("turnout_counter_message", None):
        await bot.delete_message(
            message.chat.id, state_data.get("turnout_counter_message")
        )
    keyboard = types.InlineKeyboardMarkup(resize_keyboard=True)
    keyboard.row(
        types.InlineKeyboardButton("Да", callback_data="turnout_counter_verify")
    )
    m = await message.answer(
        f"🕵️ По твоим данным явка сейчас {int(message.text.strip())}? \n"
        f"✏️ В случае опечатки, можно отправить новое число. \n\n"
        f""
        f"Если всё верно, нажми Да",
        reply_markup=keyboard,
    )
    await state.update_data(
        turnout_counter=int(message.text.strip()), turnout_counter_message=m.message_id
    )


@dp.message_handler(state=TurnoutFunnel.waiting_for_official)
async def turnout_set_counter(message: types.Message, state: FSMContext):
    if not message.text.strip().isdecimal():
        await message.answer("Пожалуйста, введите число")
        return
    state_data = await state.get_data()
    if state_data.get("turnout_official_message", None):
        await bot.delete_message(
            message.chat.id, state_data.get("turnout_official_message")
        )
    keyboard = types.InlineKeyboardMarkup(resize_keyboard=True)
    keyboard.row(
        types.InlineKeyboardButton("Да", callback_data="turnout_official_verify")
    )
    m = await message.answer(
        f"🧑‍✈ По данным УИКа явка сейчас {int(message.text.strip())}?\n"
        f"✏️ В случае опечатки, можно отправить новое число.\n\n"
        f""
        f"Если всё верно, нажми Да",
        reply_markup=keyboard,
    )
    await state.update_data(
        turnout_official=int(message.text.strip()),
        turnout_official_message=m.message_id,
    )


@dp.callback_query_handler(
    lambda c: c.data == "turnout_counter_verify",
    state=TurnoutFunnel.waiting_for_counter,
)
async def turnout_verify_counter(callback: types.CallbackQuery, state: FSMContext):
    state_data = await state.get_data()
    await callback.message.edit_text(
        f'По твоим данным явка составила {state_data["turnout_counter"]}'
    )
    await callback.message.answer("🧑‍✈️ Отправь явку, которую насчитал УИК")
    await TurnoutFunnel.waiting_for_official.set()


@dp.callback_query_handler(
    lambda c: c.data == "turnout_official_verify",
    state=TurnoutFunnel.waiting_for_official,
)
async def turnout_verify_counter(callback: types.CallbackQuery, state: FSMContext):
    state_data = await state.get_data()
    await callback.message.edit_text(
        f'По данным УИКа явка составила {state_data["turnout_official"]}'
    )
    db = get_db()
    data = {
        "user_id": callback.from_user.id,
        "time": int(time.time()),
        "counter_voters": state_data["turnout_counter"],
        "uik_voters": state_data["turnout_official"],
    }
    user = await db.observers.find_one({"user_id": callback.from_user.id})
    await db.uiks.update_one({"uik": user["uik"]}, {"$push": {"voters_history": data}})
    await db.uiks.update_one(
        {"uik": user["uik"]},
        {
            "$set": {
                "counter_voters": state_data["turnout_counter"],
                "uik_voters": state_data["turnout_official"],
            }
        },
    )
    await callback.message.answer(
        f"🕵️ Насчитано: {state_data['turnout_counter']}\n"
        f"🧑‍✈️ Официально: {state_data['turnout_official']}\n\n"
        f"Данные улетели в Штаб 🚀"
    )
    await state.reset_state()
