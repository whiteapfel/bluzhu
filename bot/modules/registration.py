from aiogram import types

from bot import dp, bot


@dp.message_handler(commands=["start"])
async def start_command(message: types.Message):
    keyboard = types.ReplyKeyboardMarkup()
    keyboard.row(
        types.KeyboardButton("Вижу нарушение"),
        types.KeyboardButton("Памятки"),
    )
    keyboard.row(
        types.KeyboardButton("Образцы заявлений"),
        types.KeyboardButton("Явка"),
    )
    # keyboard.row(types.KeyboardButton("КАРАУЛ! ТРЕВОГА!"))
    await message.reply("🧑‍🏭🧑‍🔧 Рабочие установили кнопочка ", reply_markup=keyboard)


@dp.callback_query_handler(lambda c: c.data and c.data.split("_")[0] == "registration")
async def registration_callback(callback: types.CallbackQuery):
    if callback.data.split("_")[-1] == "yes":
        await callback.message.edit_text(
            "🎉 Твоя заявка ушла на рассмотрение в штаб.\n\nТеперь надо дождаться решения"
        )
        await callback.answer("Ты попал в очередь на подтверждение")
    elif callback.data.split("_")[-1] == "no":
        await callback.answer("Мне нравится твоя честность")
    else:
        await callback.answer("Что-то пошло не по плану")
